package com.example.adriancova.githubvis.activities;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adriancova.githubvis.R;
import com.example.adriancova.githubvis.objects.Issue;
import com.example.adriancova.githubvis.objects.IssueResult;
import com.example.adriancova.githubvis.objects.User;
import com.example.adriancova.githubvis.objects.UserResult;
import com.example.adriancova.githubvis.utils.RecyclerTouchListener;
import com.example.adriancova.githubvis.adapters.RepoAdapter;
import com.example.adriancova.githubvis.fragments.DialogRepoFragment;
import com.example.adriancova.githubvis.interfaces.ClickListener;
import com.example.adriancova.githubvis.interfaces.RestClient;
import com.example.adriancova.githubvis.objects.Repository;
import com.example.adriancova.githubvis.objects.RepositoryResult;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SearchActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String URL_API = "https://api.github.com/";
    private Spinner spinnerLgs;
    private Button btnBuscarSpinner;
    private Button btnBuscar;
    private EditText etLanguage;
    private RecyclerView rView;
    private RepoAdapter rAdapter;
    private TextView txtResult;
    private String searchParam;
    private DialogRepoFragment fragmentRepo;

    private List<Repository> repositoryList = new ArrayList<>();
    private List<Issue> issueList = new ArrayList<>();
    private List<User> userList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchParam = "";
        initViews();
    }

    private void initViews(){
        spinnerLgs = (Spinner) findViewById(R.id.spinnerLenguaje);
        btnBuscarSpinner = (Button) findViewById(R.id.btnBuscarSpinner);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        etLanguage = (EditText) findViewById(R.id.txtLenguaje);
        txtResult = (TextView) findViewById(R.id.txtResult);

        rView = (RecyclerView)findViewById(R.id.rvRepositorios);
        rAdapter = new RepoAdapter(SearchActivity.this, repositoryList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rView.setLayoutManager(layoutManager);
        rView.setAdapter(rAdapter);

        rView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Repository repo = repositoryList.get(position);
                if (repo.getLastIssues() == null || repo.getMainUsers() == null){
                    loadRepositoryDetails(position);
                    Toast.makeText(SearchActivity.this,"Cargando detalles del repositorio, espere un momento", Toast.LENGTH_SHORT).show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            cargarModel(position);
                        }
                    }, 2000);
                } else {
                    cargarModel(position);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(SearchActivity.this,"Posición: " + position, Toast.LENGTH_SHORT).show();
            }
        }));
        spinnerLgs.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.langugages_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLgs.setAdapter(adapter);

        btnBuscarSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchParam = spinnerLgs.getSelectedItem().toString();
                loadRepositories(searchParam);
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchParam = etLanguage.getText().toString();
                loadRepositories(searchParam);
            }
        });
    }

    private void cargarModel(int position){
        fragmentRepo = new DialogRepoFragment(SearchActivity.this,repositoryList.get(position));
        fragmentRepo.show();
        fragmentRepo.btnSalir.setText("Salir");
        fragmentRepo.btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentRepo.dismiss();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void loadRepositories(String language){
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_API)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        String query = "+language:" + searchParam;
        RestClient restClient = retrofit.create(RestClient.class);
        Call<RepositoryResult> call = restClient.getRepositories(query,"1", "stars");

        call.enqueue(new Callback<RepositoryResult>() {
            @Override
            public void onResponse(Call<RepositoryResult> call, Response<RepositoryResult> response) {
                switch (response.code()) {
                    case 200:
                        RepositoryResult data = response.body();
                        repositoryList = data.getItems();
                        if (repositoryList.size()> 0 ) {
                            txtResult.setVisibility(View.GONE);
                            rView.setVisibility(View.VISIBLE);
                            rAdapter.swap(repositoryList);
                        }else{
                            txtResult.setVisibility(View.VISIBLE);
                            rView.setVisibility(View.GONE);
                            txtResult.setText("La Llamada al WebService no obtuvo ningún resultado, intenté con otro lenguaje.");
                        }
                        break;
                    default:
                        txtResult.setVisibility(View.VISIBLE);
                        rView.setVisibility(View.GONE);
                        txtResult.setText("La Llamada al WebService tuvo un error, asegurese de buscar un lenguaje existente.");
                        break;
                }
            }

            @Override
            public void onFailure(Call<RepositoryResult> call, Throwable t) {
                txtResult.setVisibility(View.VISIBLE);
                rView.setVisibility(View.GONE);
                txtResult.setText("Ocurrió un error al hacer la llamada al WS, asegurese de contar con conexión a internet.");
                Log.e("error", t.toString());
            }
        });
    }

    private void loadRepositoryDetails(final int position){
        Repository repo = repositoryList.get(position);
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_API)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        String query = "repo:" + repo.getOwner().getLogin() + "/" + repo.getName();
        RestClient restClient = retrofit.create(RestClient.class);
        Call<IssueResult> issueCall = restClient.getIssues(query,"created", "1");

        issueCall.enqueue(new Callback<IssueResult>() {
            @Override
            public void onResponse(Call<IssueResult> call, Response<IssueResult> response) {
                switch (response.code()) {
                    case 200:
                        IssueResult data = response.body();
                        issueList = data.getItems();
                        if (issueList.size()> 0 ) {
                            repositoryList.get(position).setLastIssues(issueList);
                        }else{
                            Toast.makeText(SearchActivity.this,"No se encontraron issues del repositorio", Toast.LENGTH_LONG).show();
                        }
                        break;
                    default:
                        Toast.makeText(SearchActivity.this,"Ocurrió un error al buscar los issues del repositorio", Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<IssueResult> call, Throwable t) {
                Toast.makeText(SearchActivity.this,"Ocurrió un error al hacer la llamada al WS de Issues, asegurese de contar con conexión a internet.", Toast.LENGTH_LONG).show();
                Log.e("error", t.toString());
            }
        });

        Call<List<UserResult>> userCall = restClient.getUsers(repo.getOwner().getLogin(),repo.getName());

        userCall.enqueue(new Callback<List<UserResult>>() {
            @Override
            public void onResponse(Call<List<UserResult>> call, Response<List<UserResult>> response) {
                switch (response.code()) {
                    case 200:
                        List<UserResult> data = response.body();
                        int total = data.size();
                        if (total > 0 ) {
                            User[] arrayUsers = new User[3];
                            arrayUsers[0] = data.get(total-1).getAuthor();
                            arrayUsers[0].setCommits(data.get(total-1).getTotal());
                            arrayUsers[1] = data.get(total-2).getAuthor();
                            arrayUsers[1].setCommits(data.get(total-2).getTotal());
                            arrayUsers[2] = data.get(total-3).getAuthor();
                            arrayUsers[2].setCommits(data.get(total-3).getTotal());
                            repositoryList.get(position).setMainUsers(arrayUsers);
                        }else{
                            Toast.makeText(SearchActivity.this,"No se encontraron usuarios del repositorio", Toast.LENGTH_LONG).show();
                        }
                        break;
                    default:
                        Toast.makeText(SearchActivity.this,"Ocurrió un error al buscar los usuarios del repositorio", Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<UserResult>> call, Throwable t) {
                Toast.makeText(SearchActivity.this,"Ocurrió un error al hacer la llamada al WS de Issues, asegurese de contar con conexión a internet.", Toast.LENGTH_LONG).show();
                Log.e("error", t.toString());
            }
        });
    }
}