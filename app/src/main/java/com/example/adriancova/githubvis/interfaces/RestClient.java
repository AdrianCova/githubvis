package com.example.adriancova.githubvis.interfaces;

import com.example.adriancova.githubvis.objects.IssueResult;
import com.example.adriancova.githubvis.objects.RepositoryResult;
import com.example.adriancova.githubvis.objects.UserResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestClient {

    @GET("search/repositories")
    Call<RepositoryResult> getRepositories(@Query(value = "q", encoded = true) String query, @Query(value = "page", encoded = true) String page, @Query(value = "sort", encoded = true) String sort);

    @GET("search/issues?per_page=3")
    Call<IssueResult> getIssues(@Query(value = "q", encoded = true) String repo, @Query(value = "sort", encoded = true) String sort, @Query(value = "page", encoded = true) String page);

    @GET("repos/{owner}/{repo}/stats/contributors")
    Call<List<UserResult>> getUsers(@Path(value = "owner", encoded = true) String owner, @Path(value = "repo", encoded = true) String repo);
}