package com.example.adriancova.githubvis.objects;

import java.util.List;

public class Repository {

    private String language;
    private String name;
    private RepositoryOwner owner;
    private String description;
    private Integer watchers;
    private List<Issue> lastIssues;
    private User[] mainUsers;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User[] getMainUsers() {
        return mainUsers;
    }

    public void setMainUsers(User[] mainUsers) {
        this.mainUsers = mainUsers;
    }

    public RepositoryOwner getOwner() {
        return owner;
    }

    public void setOwner(RepositoryOwner owner) {
        this.owner = owner;
    }

    public Integer getWatchers() {
        return watchers;
    }

    public void setWatchers(Integer watchers) {
        this.watchers = watchers;
    }

    public List<Issue> getLastIssues() {
        return lastIssues;
    }

    public void setLastIssues(List<Issue> lastIssues) {
        this.lastIssues = lastIssues;
    }
}
