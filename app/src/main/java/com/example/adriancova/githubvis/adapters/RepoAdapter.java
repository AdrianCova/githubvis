package com.example.adriancova.githubvis.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.adriancova.githubvis.R;
import com.example.adriancova.githubvis.objects.Repository;

import java.util.List;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.RepoViewHolder> {

    private List<Repository> list;
    private Context context;

    public RepoAdapter(Context context, List<Repository> lista) {
        this.context = context;
        this.list = lista;
    }

    public void swap(List<Repository> newList) {
        list.clear();
        list.addAll(newList);
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public RepoAdapter.RepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new RepoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_repositorio, parent, false));
        //return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RepoAdapter.RepoViewHolder holder, int i) {
        final Repository repo = list.get(i);
        holder.repoName.setText(repo.getName());
        holder.ownerName.setText(repo.getOwner().getLogin());
        holder.watchers.setText(repo.getWatchers().toString());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class RepoViewHolder extends RecyclerView.ViewHolder {
        TextView repoName;
        TextView ownerName;
        TextView watchers;

        public RepoViewHolder(View itemView) {
            super(itemView);
            repoName = (TextView)itemView.findViewById(R.id.repo_name);
            ownerName = (TextView)itemView.findViewById(R.id.owner_name);
            watchers = (TextView)itemView.findViewById(R.id.watchers);
        }
    }
}
