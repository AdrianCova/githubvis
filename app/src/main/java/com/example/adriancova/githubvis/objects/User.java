package com.example.adriancova.githubvis.objects;

public class User {

    private String login;
    private Integer commits;

    public String getLogin() {
        return login;
    }

    public void setName(String login) {
        this.login = login;
    }

    public Integer getCommits() {
        return commits;
    }

    public void setCommits(Integer commits) {
        this.commits = commits;
    }
}
