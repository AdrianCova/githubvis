package com.example.adriancova.githubvis.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adriancova.githubvis.R;
import com.example.adriancova.githubvis.objects.Issue;
import com.example.adriancova.githubvis.objects.Repository;
import com.example.adriancova.githubvis.objects.User;
import com.example.adriancova.githubvis.utils.DownloadImageTask;

public class DialogRepoFragment extends Dialog {
    private TextView lblNombre;
    private TextView lblDescripcion;
    private TextView lblIssues;
    private TextView lblUsers;
    public Button btnSalir;

    private Context context;
    private Repository repo;

    public DialogRepoFragment(Context context, Repository repo) {
        super(context);
        this.context = context;
        this.repo=repo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_repo);

        ImageView imgRepo = (ImageView) findViewById(R.id.imgRepoDialog);
        lblNombre = (TextView) findViewById(R.id.lblNombreDialog);
        lblDescripcion = (TextView) findViewById(R.id.lblDescDialog);
        lblIssues = (TextView) findViewById(R.id.lblIssues);
        lblUsers = (TextView) findViewById(R.id.lblUsers);
        btnSalir = (Button) findViewById(R.id.btnSalirDialog);

        new DownloadImageTask(imgRepo).execute(repo.getOwner().getAvatarUrl());
        lblNombre.setText(repo.getName());
        lblDescripcion.setText(repo.getDescription());
        String issues = "Dato vacío, es probable que haya ocurrido un error al consultar los issues del repositorio";
        String users = "Dato vacío, es probable que haya ocurrido un error al consultar los usuarios del repositorio";
        if (repo.getLastIssues() != null){
            issues = "";
            for (Issue is: repo.getLastIssues()) {
                issues += "#" + is.getNumber() + " - " + is.getTitle() + System.getProperty("line.separator");
            }
        }
        if (repo.getMainUsers() != null) {
            users = "";
            for (User us: repo.getMainUsers()){
                users += us.getLogin() + " - " + us.getCommits() + " commits" + System.getProperty("line.separator");
            }

        }
        lblIssues.setText(issues);
        lblUsers.setText(users);
    }
}
